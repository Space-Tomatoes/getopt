{
	'includes': [
		'../Common.gypi'
	],
	'targets': [
		{
			'target_name': 'getopt',
			'type': 'static_library',
			'variables': {
				'source_files': [
					'src/getopt.c',
				],
				'include_files': [
					'include/getopt.h',
				],
			},
			'direct_dependent_settings': {
				'defines': [
					'STATIC_GETOPT',
				],
				'include_dirs': [
					'include',
				],
			},
			'defines': [
				'STATIC_GETOPT',
			],
			'include_dirs': [
				'include',
			],
			'sources': [ 
				'<@(source_files)', 
				'<@(include_files)',
			],
		},
	],
}